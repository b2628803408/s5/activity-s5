-- https://9to5answer.com/change-the-date-format-in-phpmyadmin

-- [SECTION] Add New Records

-- Add 5 artists, 2 albums each, 2 songs per album.

-- Add 5 artists.

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", "00:04:06", "Pop rock", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", "00:03:33", "Country pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", "00:04:33", "Rock, alternative rock, arena rock", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", "00:03:24", "Country", 4);

-- Lady Gaga

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-05", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", "00:03:01", "Rock and roll", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", "00:03:21", "Country, rock, folk rock", 5);	

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-05-23", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", "00:04:12", "Electropop", 6);

-- Justin Bieber

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", "00:03:12", "Dancehall-poptropical housemoombahton", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-06-15", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", "00:04:11", "Pop", 8);

-- Ariana Grande

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", "00:04:02", "EDM house", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-02-08", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", "00:03:16", "Pop, R&B", 10);

-- Bruno Mars

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-11-18", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", "00:03:27", "Funk, disco, R&B", 11);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-09-11", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", "00:03:12", "Pop", 12);


-- [SECTION] Advanced Selects

-- EXCLUDE records
SELECT * FROM songs WHERE id  != 15;

-- Greater than/less than
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id > 11;

-- getting a specific ids 

-- (OR)
SELECT * FROM songs WHERE id=2 OR id=3 OR id=4;

-- (AND)
-- will return false due to conflicting condition
SELECT * FROM songs WHERE id=2 AND id=3 AND id=4;

--AND OPERATOR
-- no return if one is false
SELECT * FROM songs WHERE id=6 AND genre="Kpop";

-- Getting specific IDs (IN)
SELECT * FROM songs WHERE id IN (2, 3, 6);
SELECT * FROM songs WHERE genre IN ("Pop", "Kpop");

-- Combining Conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 15;

-- find partial matches
-- the "%" (modulo) and the "_" (underscore)

-- "%" - represents all the characters that may exist before or after the given characters after the LIKE function; 

-- "_" - represents each chacracter that exist before or after the given characters after the LIKE function.

--selects keyword from the end
-- lahat ng word na may ending na 'a';
SELECT * FROM songs WHERE song_name LIKE "%a";

--first 2 letter is "bo"
SELECT * FROM songs WHERE song_name LIKE "bo%";

--select ketword in between, 
-- may 'a' sa song title sa pagitan
SELECT * FROM songs WHERE song_name LIKE "%a%";

-- underscore "_"
--double underscore
SELECT * FROM songs WHERE song_name LIKE "__d"; 

--3ple underscore
--wil lresult to a empty set
SELECT * FROM songs WHERE song_name LIKE "___w"; 

-- COMBINATION of % and _
SELECT * FROM songs WHERE song_name LIKE "%__t"; 
SELECT * FROM songs WHERE song_name LIKE "_o_%"; 

-- SORTING RECORDS
-- ASC - ascending
-- DESC - descending
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- The ORDER BY and ASC/DESC will still perform the sorting using the first field define as reference
SELECT * FROM songs ORDER BY  song_name, genre ASC;
SELECT * FROM songs ORDER BY genre, song_name ASC;

-- REMOVE DUPLICATES
-- DISTINCT function - allows us to output/return unique field or values
-- Old: will return all genre
SELECT genre FROM songs;

--New: will return all genre once, no duplicates
SELECT DISTINCT genre FROM songs;



-- [SECTION] TABLE Joins

-- Combine artists and album tables.
SELECT * FROM  artists
    JOIN albums ON artists.id = albums.artist_id;

--combine more than two tables
SELECT * FROM  artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;

SELECT * FROM  artists
    JOIN songs ON albums.id = songs.album_id;
-- ERROR 1054 (42S22): Unknown column 'albums.id' in 'on clause'



-- SELECT COLUMNS TO BE INCLUDED PER TABLE
SELECT artists.name, albums.album_title FROM artists
    JOIN albums ON artists.id  = albums.artist_id;

SELECT DISTINCT artists.name, albums.album_title FROM artists
    JOIN albums ON artists.id  = albums.artist_id;  

-- show artists without records on the right side of the join table
SELECT * FROM  artists
    LEFT JOIN albums ON artists.id = albums.artist_id;

-- RIGHT JOIN
SELECT * FROM  artists
    RIGHT JOIN albums ON artists.id = albums.artist_id;


https://joins.spathon.com/
https://devhints.io/mysql


-- A. Find all artists that have letter d in its name;

SELECT * FROM artists WHERE name LIKE "%d%";

-- B. Find all songs that have a length of less than 3:30.
SELECT * FROM songs WHERE length < "00:03:30";

-- C. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length);
SELECT albums.album_title, songs.song_name, songs.length FROM albums
    JOIN songs ON albums.id = songs.album_id;


-- D. Join that 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";



-- E. Sort the albums in Z-A order. (Show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;



-- F. join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM albums 
    JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;


